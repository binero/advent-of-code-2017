#![feature(io)]

fn get_input() -> Vec<i32> {
    use std::io::{stdin, Read};
    let string: String = stdin().chars().map(Result::unwrap).collect();

    string
        .split_whitespace()
        .map(|word| word.parse())
        .map(Result::unwrap)
        .collect()
}

fn main() {
    let mut input = get_input();

    println!("a: {}", a(&mut input.clone()));
    println!("b: {}", b(&mut input));
}

fn a(input: &mut [i32]) -> i32 {
    let mut position = 0;
    let mut steps = 0;

    while position >= 0 && position < input.len() as i32 {
        steps += 1;
        let offset = input[position as usize];
        let new_position = position + offset;
        input[position as usize] += 1;
        std::mem::replace(&mut position, new_position);
    }

    steps
}

fn b(input: &mut [i32]) -> i32 {
    let mut position = 0;
    let mut steps = 0;

    while position >= 0 && position < input.len() as i32 {
        steps += 1;
        let offset = input[position as usize];
        let new_position = position + offset;
        input[position as usize] += if offset < 3 { 1 } else { -1 };
        std::mem::replace(&mut position, new_position);
    }

    steps
}
