extern crate fnv;
extern crate smallvec;

use smallvec::SmallVec;

pub fn tick(banks: &mut [u8]) {
    let highest_index = (0..banks.len())
        .rev()
        .max_by_key(|&index| banks[index])
        .unwrap();
    let todo = banks[highest_index] as usize;
    banks[highest_index] = 0;

    let todo_flat = todo / banks.len();
    let todo_extra = todo % banks.len();

    for element in banks.iter_mut() {
        *element += todo_flat as u8;
    }

    for index in std::iter::repeat(0..banks.len())
        .flat_map(|me| me)
        .skip(highest_index + 1)
        .take(todo_extra)
    {
        banks[index] += 1;
    }
}

pub fn find_cycle(banks: &mut [u8]) -> (usize, usize) {
    use fnv::FnvHashMap;

    let mut all_previous_banks: FnvHashMap<SmallVec<[u8; 16]>, u16> =
        FnvHashMap::with_capacity_and_hasher(15000, Default::default());

    all_previous_banks.insert(SmallVec::from_slice(banks), 0);

    (1..)
        .filter_map(|step| {
            tick(banks);
            if let Some(old_steps) =
                all_previous_banks.insert(SmallVec::from_slice(banks), step as u16)
            {
                Some((step, step - old_steps as usize))
            } else {
                None
            }
        })
        .next()
        .unwrap()
}
