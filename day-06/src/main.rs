#![feature(io)]

extern crate day6;
extern crate smallvec;

use smallvec::SmallVec;

fn main() {
    use std::io::{stdin, Read};
    let input: String = stdin().chars().map(Result::unwrap).collect();
    let mut banks: SmallVec<[u8; 16]> = input
        .split_whitespace()
        .map(|word| word.parse().unwrap())
        .collect();

    let (a, b) = day6::find_cycle(&mut banks);
    println!("a: {}", a);
    println!("b: {}", b);
}
