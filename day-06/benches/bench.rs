#![feature(test)]

extern crate day6;
extern crate rand;
extern crate smallvec;
extern crate test;

use test::{black_box, Bencher};
use smallvec::SmallVec;

const REAL_INPUTS: [([u8; 16], usize, usize); 2] = [
    (
        [5, 1, 10, 0, 1, 7, 13, 14, 3, 12, 8, 10, 7, 12, 0, 6],
        5042,
        1086,
    ),
    (
        [14, 0, 15, 12, 11, 11, 3, 5, 1, 6, 8, 4, 9, 1, 8, 4],
        11137,
        1037,
    ),
];

#[bench]
fn bench_real_ab(bencher: &mut Bencher) {
    bencher.iter(|| {
        for (mut banks, a, b) in black_box(REAL_INPUTS).iter().map(|input| input.clone()) {
            assert_eq!((a, b), day6::find_cycle(&mut banks));
        }
    });
}

#[bench]
fn bench_random_ab_8(bencher: &mut Bencher) {
    let inputs = get_random_inputs(128, 8);

    bencher.iter(move || {
        for mut banks in inputs.iter().cloned() {
            let _ = black_box(day6::find_cycle(&mut banks));
        }
    });
}

#[bench]
fn bench_random_ab_16(bencher: &mut Bencher) {
    let inputs = get_random_inputs(128, 16);

    bencher.iter(move || {
        for mut banks in inputs.iter().cloned() {
            let _ = black_box(day6::find_cycle(&mut banks));
        }
    });
}

#[bench]
fn bench_random_ab_32(bencher: &mut Bencher) {
    let inputs = get_random_inputs(128, 32);

    bencher.iter(move || {
        for mut banks in inputs.iter().cloned() {
            let _ = black_box(day6::find_cycle(&mut banks));
        }
    });
}

fn get_random_inputs(amount: usize, length: usize) -> Vec<SmallVec<[u8; 16]>> {
    use rand::{thread_rng, Rng};

    let mut rng = thread_rng();
    (0..amount).map(|_| rng.gen_iter().take(length).collect()).collect()
}