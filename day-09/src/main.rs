#![feature(io)]

fn main() {
    use std::io::{stdin, Read};

    let mut input = stdin().chars().map(Result::unwrap);
    assert_eq!(input.next(), Some('{'));

    let group = Item::Group(parse(&mut input));
    println!("a: {}", group.calculate_score());
    println!("b: {}", group.count_garbage());
}

pub enum Item {
    Garbage(String),
    Group(Vec<Item>),
}

impl Item {
    fn calculate_score(&self) -> u32 {
        self.calculate_score_inner(1)
    }

    fn calculate_score_inner(&self, base: u32) -> u32 {
        if let &Item::Group(ref group) = self {
            group
                .iter()
                .map(|sub_group| sub_group.calculate_score_inner(base + 1))
                .sum::<u32>() + base
        } else {
            0
        }
    }

    fn count_garbage(&self) -> u32 {
        match self {
            &Item::Group(ref group) => group.iter().map(Item::count_garbage).sum(),
            &Item::Garbage(ref garbage) => garbage.len() as u32,
        }
    }
}

fn parse<I: Iterator<Item = char>>(it: &mut I) -> Vec<Item> {
    let mut output = Vec::new();
    loop {
        match it.next() {
            Some('<') => output.push(Item::Garbage(parse_garbage(it))),
            Some('{') => output.push(Item::Group(parse(it))),
            Some('}') => return output,
            Some(',') => (),
            Some(oth) => panic!("Invalid input '{}'!", oth),
            None => panic!("Unexpected end of input!"),
        }
    }
}

fn parse_garbage<I: Iterator<Item = char>>(it: &mut I) -> String {
    let mut garbage = String::new();
    loop {
        let next = it.next().expect("Unexpected end of input");
        match next {
            '!' => {
                it.next().unwrap();
            }
            '>' => return garbage,
            otr => garbage.push(otr),
        }
    }
}
