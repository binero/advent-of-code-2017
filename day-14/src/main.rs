extern crate day_10;

use std::fmt::{Debug, Formatter, Result as FmtResult};

fn main() {
    use std::env::args;

    let input = args().skip(1).next().expect("no puzzle input");
    let map: Map = input.as_str().into();
    println!("{:?}", map);
    println!("a: {}", map.occupied_space());
    println!("b: {}", map.count_groups());
}

pub struct Map {
    inner: [[bool; Map::SIZE]; Map::SIZE],
}

impl Map {
    const SIZE: usize = 128;

    pub fn occupied_space(&self) -> usize {
        self.inner
            .iter()
            .flat_map(|row| row.iter())
            .filter(|&&cel| cel)
            .count()
    }

    pub fn count_groups(mut self) -> usize {
        let all_coords = (0..Map::SIZE).flat_map(|x| (0..Map::SIZE).map(move |y| (x, y)));
        all_coords.filter(|&(x, y)| self.remove_group(x, y)).count()
    }

    fn remove_group(&mut self, x: usize, y: usize) -> bool {
        if self.inner[y][x] {
            self.inner[y][x] = false;

            if x > 0 {
                self.remove_group(x - 1, y);
            }

            if y > 0 {
                self.remove_group(x, y - 1);
            }

            if x + 1 < Map::SIZE {
                self.remove_group(x + 1, y);
            }

            if y + 1 < Map::SIZE {
                self.remove_group(x, y + 1);
            }

            true
        } else {
            false
        }
    }
}

impl<'s> From<&'s str> for Map {
    fn from(s: &'s str) -> Self {
        use day_10::Ring;

        let mut map = unsafe {
            Map {
                inner: std::mem::uninitialized(),
            }
        };

        for (row_index, row) in map.inner.iter_mut().enumerate() {
            let input = format!("{}-{}", s, row_index);

            let hash = {
                let mut ring = Ring::default();
                for _ in 0..64 {
                    for byte in input.bytes() {
                        ring.add_input(byte);
                    }

                    for &byte in [17, 31, 73, 47, 23].iter() {
                        ring.add_input(byte);
                    }
                }
                ring.generate_dense_hash()
            };

            let unsized_row = hash.iter()
                .cloned()
                .flat_map(|byte| (0..8).rev().map(move |offset| ((1 << offset) & byte) != 0));

            for (cell, cell_source) in row.iter_mut().zip(unsized_row) {
                *cell = cell_source
            }
        }

        map
    }
}

impl Debug for Map {
    fn fmt(&self, formatter: &mut Formatter) -> FmtResult {
        for row in self.inner.iter() {
            for &col in row.iter() {
                if col {
                    write!(formatter, "#")?;
                } else {
                    write!(formatter, ".")?;
                }
            }
            writeln!(formatter)?;
        }
        Ok(())
    }
}
