extern crate day1;

fn main() {
    let integers = day1::get_input_integers();
    let (first_half, second_half) = integers.split_at(integers.len() / 2);

    assert_eq!(
        first_half.len(),
        second_half.len(),
        "There must be an even number of input integers.",
    );

    let sum: u32 = first_half
        .iter()
        .zip(second_half.iter())
        .filter(|&(l, r)| l == r)
        .map(|(l, _)| (l + l) as u32)
        .sum();

    println!("{}", sum);
}
