extern crate day1;

fn main() {
    let integers = day1::get_input_integers();

    let mut sum: u32 = integers
        .windows(2)
        .filter(|slice| slice[0] == slice[1])
        .map(|slice| slice[0] as u32)
        .sum();

    if integers.first() == integers.last() {
        sum += *integers.first().expect("No elements in standard output.") as u32;
    }

    println!("{}", sum);
}
