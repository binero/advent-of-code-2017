#![feature(io)]

pub fn get_input_integers() -> Vec<u8> {
    use std::io::{stdin, Read};

    stdin()
        .chars()
        .filter_map(|ch| ch.unwrap().to_digit(10))
        .map(|d| d as u8)
        .collect()
}
