pub trait IteratorExtSplit: Sized + Iterator<Item = char> {
    fn split(self, on: char) -> IterSpliter<Self>;
}

impl<It> IteratorExtSplit for It
where
    It: Sized + Iterator<Item = char>,
{
    fn split(self, on: char) -> IterSpliter<Self> {
        IterSpliter {
            inner_iter: self,
            split_on: on,
        }
    }
}

pub struct IterSpliter<It: Iterator<Item = char>> {
    inner_iter: It,
    split_on: char,
}

impl<It> Iterator for IterSpliter<It>
where
    It: Iterator<Item = char>,
{
    type Item = String;

    fn next(&mut self) -> Option<Self::Item> {
        let mut to_return = String::new();
        loop {
            match self.inner_iter.next() {
                Some(ch) if ch == self.split_on => return Some(to_return),
                Some(ch) => to_return.push(ch),
                None => {
                    return if to_return.len() == 0 {
                        None
                    } else {
                        Some(to_return)
                    }
                }
            }
        }
    }
}
