#![feature(io)]

#[macro_use]
extern crate lazy_static;
extern crate regex;

mod dance;
mod splitter;
mod stage;

use splitter::IteratorExtSplit;
use dance::DanceMove;
use stage::Stage;

fn main() {
    use std::io::{stdin, Read};

    let dance_moves: Vec<_> = stdin()
        .chars()
        .map(Result::unwrap)
        .filter(|ch| !ch.is_whitespace())
        .split(',')
        .map(|instruction| instruction.parse().unwrap())
        .collect();

    println!("{:?}", a(dance_moves.iter().cloned()));
    println!("{:?}", b(&dance_moves));
}

fn a<I: Iterator<Item = DanceMove>>(dance_moves: I) -> Stage {
    let mut stage = Stage::default();

    for dance_move in dance_moves {
        stage.perform(&dance_move);
    }

    stage
}

fn b(dance_moves: &[DanceMove]) -> Stage {
    use std::collections::HashMap;

    const DANCE_COUNT: u64 = 1_000_000_000;

    let mut stage = Stage::default();
    let mut previous_stages = HashMap::new();

    for iteration in 0..DANCE_COUNT {
        for dance_move in dance_moves {
            stage.perform(dance_move);
        }

        if let Some(first_seen_index) = previous_stages.insert(stage.clone(), iteration) {
            // There is only 65536 different stage configurations, so we are guaranteed to hit a
            // duplicate.
            let cycle_size = iteration - first_seen_index;
            let todo = DANCE_COUNT - iteration - 1;
            let shortcut = DANCE_COUNT - todo % cycle_size;

            for _ in shortcut..DANCE_COUNT {
                for dance_move in dance_moves {
                    stage.perform(dance_move);
                }
            }

            break;
        }
    }

    stage
}
