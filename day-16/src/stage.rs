use dance::{DanceMove, Program};

#[derive(Debug, Eq, PartialEq, Hash, Clone)]
pub struct Stage {
    inner: [Program; Stage::SIZE],
}

impl Stage {
    const SIZE: usize = 16;

    pub fn perform(&mut self, dance_move: &DanceMove) {
        match dance_move {
            &DanceMove::Spin(amount) => self.spin(amount),
            &DanceMove::Swap(x, y) => self.swap(x, y),
            &DanceMove::Partner(x, y) => self.partner(x, y),
        }
    }

    fn swap(&mut self, x: usize, y: usize) {
        self.inner.swap(x, y);
    }

    fn find_program_index(&self, program: Program) -> usize {
        self.inner
            .iter()
            .enumerate()
            .find(|&(_, &candidate)| candidate == program)
            .map(|(index, _)| index)
            .unwrap()
    }

    fn partner(&mut self, x: Program, y: Program) {
        let x = self.find_program_index(x);
        let y = self.find_program_index(y);
        self.swap(x, y);
    }

    fn spin(&mut self, amount: usize) {
        self.inner = [
            self.inner[(00 - amount as isize) as usize % self.inner.len()],
            self.inner[(01 - amount as isize) as usize % self.inner.len()],
            self.inner[(02 - amount as isize) as usize % self.inner.len()],
            self.inner[(03 - amount as isize) as usize % self.inner.len()],
            self.inner[(04 - amount as isize) as usize % self.inner.len()],
            self.inner[(05 - amount as isize) as usize % self.inner.len()],
            self.inner[(06 - amount as isize) as usize % self.inner.len()],
            self.inner[(07 - amount as isize) as usize % self.inner.len()],
            self.inner[(08 - amount as isize) as usize % self.inner.len()],
            self.inner[(09 - amount as isize) as usize % self.inner.len()],
            self.inner[(10 - amount as isize) as usize % self.inner.len()],
            self.inner[(11 - amount as isize) as usize % self.inner.len()],
            self.inner[(12 - amount as isize) as usize % self.inner.len()],
            self.inner[(13 - amount as isize) as usize % self.inner.len()],
            self.inner[(14 - amount as isize) as usize % self.inner.len()],
            self.inner[(15 - amount as isize) as usize % self.inner.len()],
        ];
    }
}

impl Default for Stage {
    fn default() -> Self {
        Stage {
            inner: [
                Program::A,
                Program::B,
                Program::C,
                Program::D,
                Program::E,
                Program::F,
                Program::G,
                Program::H,
                Program::I,
                Program::J,
                Program::K,
                Program::L,
                Program::M,
                Program::N,
                Program::O,
                Program::P,
            ],
        }
    }
}
