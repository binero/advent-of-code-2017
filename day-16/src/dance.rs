use std::str::FromStr;
use regex::Regex;
use std::error::Error;
use std::fmt::{Display, Formatter, Result as FmtResult};

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum Program {
    A,
    B,
    C,
    D,
    E,
    F,
    G,
    H,
    I,
    J,
    K,
    L,
    M,
    N,
    O,
    P,
}

impl Program {
    fn from(from: char) -> Option<Program> {
        match from {
            'a' => Some(Program::A),
            'b' => Some(Program::B),
            'c' => Some(Program::C),
            'd' => Some(Program::D),
            'e' => Some(Program::E),
            'f' => Some(Program::F),
            'g' => Some(Program::G),
            'h' => Some(Program::H),
            'i' => Some(Program::I),
            'j' => Some(Program::J),
            'k' => Some(Program::K),
            'l' => Some(Program::L),
            'm' => Some(Program::M),
            'n' => Some(Program::N),
            'o' => Some(Program::O),
            'p' => Some(Program::P),
            _ => None,
        }
    }
}

#[derive(Debug, Copy, Clone)]
pub enum DanceMove {
    Spin(usize),
    Swap(usize, usize),
    Partner(Program, Program),
}

lazy_static! {
    static ref SPIN_REGEX: Regex = Regex::new(r"^s(?P<amount>\d+)$").unwrap();
    static ref SWAP_REGEX: Regex = Regex::new(r"^x(?P<pos1>\d+)/(?P<pos2>\d+)$").unwrap();
    static ref PARTNER_REGEX: Regex = Regex::new(r"^p(?P<prog1>[a-p])/(?P<prog2>[a-p])$").unwrap();
}

impl FromStr for DanceMove {
    type Err = DanceMoveParseError;

    fn from_str(s: &str) -> Result<DanceMove, Self::Err> {
        if let Some(captures) = SPIN_REGEX.captures(s) {
            Ok(DanceMove::Spin(
                captures.name("amount").unwrap().as_str().parse().unwrap(),
            ))
        } else if let Some(captures) = SWAP_REGEX.captures(s) {
            Ok(DanceMove::Swap(
                captures.name("pos1").unwrap().as_str().parse().unwrap(),
                captures.name("pos2").unwrap().as_str().parse().unwrap(),
            ))
        } else if let Some(captures) = PARTNER_REGEX.captures(s) {
            Ok(DanceMove::Partner(
                Program::from(
                    captures
                        .name("prog1")
                        .unwrap()
                        .as_str()
                        .chars()
                        .next()
                        .unwrap(),
                ).unwrap(),
                Program::from(
                    captures
                        .name("prog2")
                        .unwrap()
                        .as_str()
                        .chars()
                        .next()
                        .unwrap(),
                ).unwrap(),
            ))
        } else {
            Err(DanceMoveParseError::new(s.to_string()))
        }
    }
}

#[derive(Debug, Clone)]
pub struct DanceMoveParseError {
    pub string: String,
}

impl DanceMoveParseError {
    fn new(string: String) -> Self {
        DanceMoveParseError { string: string }
    }
}

impl Error for DanceMoveParseError {
    fn description(&self) -> &str {
        "failed to parse dance move"
    }
}

impl Display for DanceMoveParseError {
    fn fmt(&self, formatter: &mut Formatter) -> FmtResult {
        write!(formatter, "'{}' is not a valid dance move.", self.string)
    }
}
