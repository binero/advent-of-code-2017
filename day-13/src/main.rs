fn main() {
    use std::io::{stdin, BufRead};

    let stdin = stdin();
    let lines = stdin.lock().lines().map(Result::unwrap);
    let scanner_data = lines.map(|word| parse_scanner_data(&word));

    let mut firewall = Vec::new();

    for (index, range) in scanner_data {
        use std::iter::repeat;
        let padding = index - firewall.len();
        firewall.extend(repeat(0).take(padding));
        firewall.push(range);
    }

    println!("a: {}", test_detection_level(firewall.iter().cloned(), 0));

    let best_delay = (10..)
        .take(1024 * 1024 * 1024)
        .filter(|&delay| !is_detected(firewall.iter().cloned(), delay))
        .next()
        .expect("No solution found");

    println!("b: {}", best_delay);
}

fn parse_scanner_data(s: &str) -> (usize, usize) {
    let mut words = s.split(':').map(|w| w.trim());
    (
        words.next().unwrap().parse().unwrap(),
        words.next().unwrap().parse().unwrap(),
    )
}

fn is_detected<I: Iterator<Item = usize>>(firewall: I, delay: usize) -> bool {
    firewall
        .enumerate()
        .map(|(depth, range)| (depth + delay, depth, range))
        .filter(|&(_, _, range)| range != 0)
        .filter(|&(timing, _, range)| timing % (range * 2 - 2) == 0)
        .next()
        .is_some()
}

fn test_detection_level<I: Iterator<Item = usize>>(firewall: I, delay: usize) -> usize {
    firewall
        .enumerate()
        .map(|(depth, range)| (depth + delay, depth, range))
        .filter(|&(_, _, range)| range != 0)
        .filter(|&(timing, _, range)| timing % (range * 2 - 2) == 0)
        .map(|(_, depth, range)| depth * range)
        .sum()
}
