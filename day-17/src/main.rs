fn main() {
    let jump_distance: usize = std::env::args().skip(1).next().unwrap().parse().unwrap();
    println!("a: {}", a(jump_distance));
    println!("b: {}", b(jump_distance));
}

fn a(jump_distance: usize) -> usize {
    use std::collections::VecDeque;

    const ROUNDS: usize = 2017;

    let mut ring = VecDeque::with_capacity(ROUNDS + 1);
    ring.push_back(0);
    let mut position = 1;

    for value in 1..(ROUNDS + 1) {
        position = (position + jump_distance) % ring.len() + 1;
        ring.insert(position, value); 
    }

    ring[(position + 1) % ring.len()]
}

fn b(jump_distance: usize) -> usize {
    const ROUNDS: usize = 50_000_000;

    let mut position = 0;
    let mut behind_0 = 0;

    for value in 1..(ROUNDS+1) {
        position = (position + jump_distance + 1) % value;

        if position == 0 {
            behind_0 = value;
        }
    }

    return behind_0;
}
