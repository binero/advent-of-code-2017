extern crate regex;

fn main() {
    use regex::Regex;
    use std::io::{stdin, BufRead};
    let regex = Regex::new(include_str!("regex")).unwrap();
    let stdin = stdin();
    let lines = stdin.lock().lines().map(Result::unwrap);
    let mut particles: Vec<_> = lines
        .map(|line| {
            let captures = regex.captures(&*line).unwrap();
            let px = captures.name("px").unwrap().as_str().parse().unwrap();
            let py = captures.name("py").unwrap().as_str().parse().unwrap();
            let pz = captures.name("pz").unwrap().as_str().parse().unwrap();
            let vx = captures.name("vx").unwrap().as_str().parse().unwrap();
            let vy = captures.name("vy").unwrap().as_str().parse().unwrap();
            let vz = captures.name("vz").unwrap().as_str().parse().unwrap();
            let ax = captures.name("ax").unwrap().as_str().parse().unwrap();
            let ay = captures.name("ay").unwrap().as_str().parse().unwrap();
            let az = captures.name("az").unwrap().as_str().parse().unwrap();
            Particle {
                position: (px, py, pz),
                velocity: (vx, vy, vz),
                acceleration: (ax, ay, az),
            }
        })
        .enumerate()
        .collect();

    println!(
        "a: {:?}",
        get_closest_particle(&mut particles.iter().cloned())
    );

    for _ in 0..100 {
        // I tried a really clever design using quadratic equations and calulating the roots, but
        // that failed. This'll do.
        use std::collections::HashMap;
        let mut seen_map = HashMap::with_capacity(particles.len());

        for &(_index, ref particle) in &particles {
            *seen_map.entry(particle.position).or_insert(0) += 1;
        }

        for index in (0..particles.len()).rev() {
            let seen_count = {
                let &(_index, ref particle) = &particles[index];
                seen_map[&particle.position]
            };
            if seen_count > 1 {
                particles.swap_remove(index);
            }
        }

        for &mut (_, ref mut particle) in &mut particles {
            *particle = particle.tick();
        }
    }

    println!("b: {}", particles.len());
}

fn get_closest_particle(particles: &mut Iterator<Item = (usize, Particle)>) -> usize {
    particles
        .min_by_key(|&(_, ref particle)| particle.get_acceleration_norm())
        .map(|(index, _)| index)
        .unwrap()
}

#[derive(Debug, Clone)]
pub struct Particle {
    pub position: (i32, i32, i32),
    pub velocity: (i32, i32, i32),
    pub acceleration: (i32, i32, i32),
}

impl Particle {
    pub fn get_acceleration_norm(&self) -> u32 {
        let (x, y, z) = self.acceleration;
        x.abs() as u32 + y.abs() as u32 + z.abs() as u32
    }

    pub fn tick(&self) -> Particle {
        let (px, py, pz) = self.position;
        let (vx, vy, vz) = self.velocity;
        let (ax, ay, az) = self.acceleration;
        let (vx, vy, vz) = (vx + ax, vy + ay, vz + az);
        let (px, py, pz) = (px + vx, py + vy, pz + vz);

        Particle {
            position: (px, py, pz),
            velocity: (vx, vy, vz),
            acceleration: (ax, ay, az),
        }
    }
}
