extern crate petgraph;

use petgraph::graphmap::UnGraphMap;
use petgraph::visit::Bfs;
use std::collections::HashSet;

fn main() {
    use std::io::{stdin, BufRead};
    let stdin = stdin();
    let nodes = stdin.lock().lines().map(Result::unwrap).map(|word| {
        let mut words = word.split("<->");
        let source: u32 = (&mut words).next().unwrap().trim().parse().unwrap();
        let sinks: Vec<u32> = (&mut words)
            .next()
            .unwrap()
            .split(',')
            .map(|word| word.trim().parse().unwrap())
            .collect();
        (source, sinks)
    });

    let mut graph = UnGraphMap::new();

    for (source, sinks) in nodes {
        for sink in sinks {
            graph.add_edge(source, sink, ());
        }
    }

    let mut bfs = Bfs::new(&graph, 0);
    let mut amount = 0;
    while let Some(_) = bfs.next(&graph) {
        amount += 1;
    }

    println!("a: {}", amount);


    let mut group_count = 0;

    while graph.node_count() > 0 {
        group_count += 1;
        let mut to_remove = HashSet::new();
        {
            let mut bfs = Bfs::new(&graph, graph.nodes().next().unwrap());
            while let Some(node) = bfs.next(&graph) {
                to_remove.insert(node);
            }
        }
        for node in to_remove {
            graph.remove_node(node);
        }
    }

    println!("b: {}", group_count);
}
