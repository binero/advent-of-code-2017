extern crate failure;
#[macro_use]
extern crate failure_derive;
extern crate itertools;

mod rule;
use rule::{Pattern, Rule};

fn main() {
    use std::io::{stdin, BufRead};

    let stdin = stdin();
    let rules = stdin
        .lock()
        .lines()
        .map(|line| line.unwrap().parse().unwrap())
        .collect::<Vec<Rule>>();

    {
        let mut pattern: Pattern = ".#./..#/###".parse().unwrap();
        for _ in 0..5 {
            pattern = pattern.enhance_resolution(&rules);
        }
        println!("a: {}", pattern.on_count());
    }

    {
        let mut pattern: Pattern = ".#./..#/###".parse().unwrap();
        for _ in 0..18 {
            pattern = pattern.enhance_resolution(&rules);
        }
        println!("b: {}", pattern.on_count());
    }
}
