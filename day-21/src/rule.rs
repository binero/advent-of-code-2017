use std::str::FromStr;
use failure::Error;
use std::fmt;

pub struct Rule {
    input: Pattern,
    output: Pattern,
}

impl FromStr for Rule {
    type Err = Error;
    fn from_str(s: &str) -> Result<Self, Error> {
        let mut patterns = s.split("=>")
            .map(|pattern| pattern.trim().parse::<Pattern>());

        match (patterns.next(), patterns.next()) {
            (Some(p1), Some(p2)) => {
                let input = p1?.into_master();
                let output = p2?;

                if input.side + 1 != output.side {
                    Err(BadRuleSizeError {
                        input_size: input.side,
                        output_size: output.side,
                    }.into())
                } else {
                    Ok(Rule { input, output })
                }
            }
            _ => Err(InvalidRuleError {
                rule: s.to_string(),
            }.into()),
        }
    }
}

impl Rule {
    pub fn apply(&self, pattern: &Pattern) -> Option<Pattern> {
        if &self.input == pattern {
            Some(self.output.clone())
        } else {
            None
        }
    }
}

#[derive(Eq, PartialEq, Ord, PartialOrd, Clone, Debug)]
pub struct Pattern {
    side: usize,
    pattern: Vec<bool>,
}

impl fmt::Display for Pattern {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        for x in 0..self.side {
            if self.at(x, 0) {
                write!(formatter, "#")?;
            } else {
                write!(formatter, ".")?;
            }
        }

        for y in 1..self.side {
            write!(formatter, "/")?;
            for x in 0..self.side {
                if self.at(x, y) {
                    write!(formatter, "#")?;
                } else {
                    write!(formatter, ".")?;
                }
            }
        }

        Ok(())
    }
}

impl Pattern {
    pub fn on_count(&self) -> usize {
        self.pattern.iter().filter(|&&is_on| is_on).count()
    }

    fn into_master(&self) -> Pattern {
        let all_variations = &mut self.all_variations();
        all_variations.sort();
        all_variations[0].clone()
    }

    fn flip_x(&self) -> Pattern {
        Pattern {
            side: self.side,
            pattern: (0..self.side)
                .rev()
                .flat_map(|y| (0..self.side).map(move |x| (x, y)))
                .map(|(x, y)| self.at(x, y))
                .collect(),
        }
    }

    fn flip_y(&self) -> Pattern {
        Pattern {
            side: self.side,
            pattern: (0..self.side)
                .flat_map(|y| (0..self.side).rev().map(move |x| (x, y)))
                .map(|(x, y)| self.at(x, y))
                .collect(),
        }
    }

    fn rotate(&self) -> Pattern {
        Pattern {
            side: self.side,
            pattern: (0..self.side)
                .flat_map(|y| (0..self.side).map(move |x| (x, y)))
                .map(|(x, y)| (y, self.side - x - 1))
                .map(|(x, y)| self.at(x, y))
                .collect(),
        }
    }

    fn all_variations(&self) -> [Pattern; 8] {
        let flipped_x = [&self.flip_x(), &self];

        let flipped_y = [
            &flipped_x[0].flip_y(),
            flipped_x[0],
            &flipped_x[1].flip_y(),
            flipped_x[1],
        ];

        [
            flipped_y[0].rotate(),
            flipped_y[0].clone(),
            flipped_y[1].rotate(),
            flipped_y[1].clone(),
            flipped_y[2].rotate(),
            flipped_y[2].clone(),
            flipped_y[3].rotate(),
            flipped_y[3].clone(),
        ]
    }

    fn at(&self, x: usize, y: usize) -> bool {
        self.pattern[x + y * self.side]
    }

    pub fn enhance_resolution(&self, rules: &[Rule]) -> Pattern {
        let group_size = if self.side % 2 == 0 {
            2
        } else if self.side % 3 == 0 {
            3
        } else {
            unimplemented!()
        };

        let new_group_size = group_size + 1;
        let sub_pattern_count = self.side / group_size;
        let new_side = sub_pattern_count * new_group_size;

        let old_groups = (0..sub_pattern_count)
            .flat_map(|y_offset| (0..sub_pattern_count).map(move |x_offset| (x_offset, y_offset)))
            .map(|(x_offset, y_offset)| (x_offset * group_size, y_offset * group_size))
            .map(|(x_offset, y_offset)| Pattern {
                side: group_size,
                pattern: (0..group_size)
                    .flat_map(|y| (0..group_size).map(move |x| (x, y)))
                    .map(|(x, y)| self.at(x_offset + x, y_offset + y))
                    .collect(),
            });

        let new_groups = old_groups
            .map(|group| group.into_master())
            .map(|ref group| {
                rules
                    .iter()
                    .filter_map(|rule| rule.apply(group))
                    .next()
                    .unwrap_or_else(|| panic!("no patterns match {}", group))
            })
            .collect::<Vec<Pattern>>();

        let pattern = (0..new_side)
            .flat_map(|y| (0..new_side).map(move |x| (x, y)))
            .map(|(x, y)| {
                let sub_pattern_x = x / new_group_size;
                let sub_pattern_y = y / new_group_size;
                let pattern_index = sub_pattern_y * sub_pattern_count + sub_pattern_x;
                new_groups[pattern_index].at(x % new_group_size, y % new_group_size)
            })
            .collect();
        Pattern {
            side: new_side,
            pattern,
        }
    }
}

impl FromStr for Pattern {
    type Err = Error;
    fn from_str(s: &str) -> Result<Self, Error> {
        let pattern = s.chars().filter(|&ch| ch != '/').map(|ch| match ch {
            '.' => Ok(false),
            '#' => Ok(true),
            character => Err(PatternTokenError { character }),
        });

        use itertools::process_results;

        let pattern: Vec<_> = process_results(pattern, |pattern| pattern.collect())?;
        let side = (pattern.len() as f64).sqrt() as usize;

        if side.pow(2) != pattern.len() {
            Err(BadPatternSizeError {
                size: pattern.len(),
            }.into())
        } else {
            Ok(Pattern { pattern, side })
        }
    }
}

#[derive(Debug, Fail)]
#[fail(display = "Unexpected character in pattern: {}", character)]
pub struct PatternTokenError {
    character: char,
}

#[derive(Debug, Fail)]
#[fail(display = "Found a pattern that is not square, size: {}", size)]
pub struct BadPatternSizeError {
    size: usize,
}

#[derive(Debug, Fail)]
#[fail(display = "Not a valid rule: {}", rule)]
pub struct InvalidRuleError {
    rule: String,
}

#[derive(Debug, Fail)]
#[fail(display = "The input side size of a rule must be exactly one larger than the output pattern side size. Found sizes: {} and {}", input_size, output_size)]
pub struct BadRuleSizeError {
    input_size: usize,
    output_size: usize,
}
