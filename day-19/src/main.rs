fn main() {
    let map = Map::from_stdin();

    let result_a: String = map.walk()
        .filter_map(|opt_found_char| opt_found_char)
        .collect();
    println!("a: {}", result_a);

    let result_b: usize = map.walk().count();
    println!("a: {}", result_b);
}

pub struct Map {
    inner: Vec<Vec<PathSegment>>,
    width: isize,
    height: isize,
}

impl Map {
    pub fn from_stdin() -> Map {
        use std::io::{stdin, BufRead};
        let stdin = stdin();
        let lines = stdin.lock().lines().map(Result::unwrap);
        let inner: Vec<Vec<_>> = lines
            .map(|line| line.chars().map(|ch| ch.into()).collect())
            .collect();

        Map {
            height: inner.len() as isize,
            width: inner[0].len() as isize,
            inner,
        }
    }

    pub fn get_at(&self, x: isize, y: isize) -> PathSegment {
        if x >= 0 && y >= 0 && x < self.width && y < self.height {
            self.inner[y as usize][x as usize]
        } else {
            PathSegment::Empty
        }
    }

    pub fn walk(&self) -> MapWalker {
        let (start_x, _) = self.inner[0]
            .iter()
            .enumerate()
            .find(|&(_index, &segment)| segment != PathSegment::Empty)
            .unwrap();

        MapWalker {
            map: &self,
            next_position: (start_x as isize, 0),
            velocity: (0, 1),
        }
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum PathSegment {
    Plain,
    Filled(char),
    Empty,
}

impl From<char> for PathSegment {
    fn from(ch: char) -> PathSegment {
        if ch.is_whitespace() {
            PathSegment::Empty
        } else if ch.is_alphabetic() {
            PathSegment::Filled(ch)
        } else {
            PathSegment::Plain
        }
    }
}

pub struct MapWalker<'m> {
    map: &'m Map,
    next_position: (isize, isize),
    velocity: (isize, isize),
}

impl<'m> Iterator for MapWalker<'m> {
    type Item = Option<char>;

    fn next(&mut self) -> Option<Option<char>> {
        let (x, y) = self.next_position;
        let to_return = match self.map.get_at(x, y) {
            PathSegment::Filled(ch) => Some(ch),
            PathSegment::Plain => None,
            PathSegment::Empty => return None,
        };

        let (velocity_x, velocity_y) = self.velocity;
        let (next_x, next_y) = (x + velocity_x, y + velocity_y);
        if self.map.get_at(next_x, next_y) == PathSegment::Empty {
            let (next_x, next_y) = (x + velocity_y, y + velocity_x);
            if self.map.get_at(next_x, next_y) == PathSegment::Empty {
                self.next_position = (x - velocity_y, y - velocity_x);
                self.velocity = (-velocity_y, -velocity_x);
            } else {
                self.next_position = (next_x, next_y);
                self.velocity = (velocity_y, velocity_x);
            }
        } else {
            self.next_position = (next_x, next_y);
        }

        Some(to_return)
    }
}
