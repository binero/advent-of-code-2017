fn main() {
    use std::io::{stdin, BufRead};

    let stdin = stdin();

    let result: i32 = stdin
        .lock()
        .lines()
        .map(Result::unwrap)
        .map(parse_line)
        .map(Option::unwrap)
        .sum();

    println!("{}", result);
}

fn parse_line(input: String) -> Option<i32> {
    input
        .split_whitespace()
        .map(|number| number.parse().unwrap())
        .fold(None, fold_min_max)
        .map(|(minv, maxv)| maxv - minv)
}

fn fold_min_max(minv_maxv: Option<(i32, i32)>, next: i32) -> Option<(i32, i32)> {
    match minv_maxv {
        Some((minv, maxv)) => Some((minv.min(next), maxv.max(next))),
        None => Some((next, next)),
    }
}
