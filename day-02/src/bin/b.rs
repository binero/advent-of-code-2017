fn main() {
    use std::io::{stdin, BufRead};

    let stdin = stdin();

    let result: i32 = stdin
        .lock()
        .lines()
        .map(|line_res| parse_line(line_res.unwrap()).unwrap())
        .sum();

    println!("{}", result);
}

fn parse_line(input: String) -> Option<i32> {
    let numbers: Vec<i32> = input
        .split_whitespace()
        .map(|number| {
            number.parse().expect("Input must only contain numbers!")
        })
        .collect();

    let indices = (0..numbers.len())
        .flat_map(|left| (0..numbers.len()).map(move |right| (left, right)))
        .filter(|&(left, right)| left != right);

    let mut pairs = indices.map(|(left, right)| (numbers[left], numbers[right]));

    pairs.find(|&(left, right)| left % right == 0).map(|(left,
      right)| {
        left / right
    })
}
