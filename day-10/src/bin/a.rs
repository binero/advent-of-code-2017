#![feature(io)]

extern crate day_10;

use day_10::Ring;

fn main() {
    use std::io::{stdin, Read};

    let input_string: String = stdin()
        .chars()
        .map(Result::unwrap)
        .filter(|ch| !ch.is_whitespace())
        .collect();
    let mut ring = Ring::default();

    for input in input_string.split(',').map(|word| word.parse().unwrap()) {
        ring.add_input(input);
    }

    println!("{}", ring[0] as usize * ring[1] as usize);
}
