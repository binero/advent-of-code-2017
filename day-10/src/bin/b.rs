#![feature(io)]

extern crate day_10;

use day_10::Ring;

fn main() {
    use std::io::{stdin, Read};

    let mut ring = Ring::default();
    let mut input: Vec<u8> = stdin().bytes().map(Result::unwrap).collect();
    input.extend_from_slice(&[17, 31, 73, 47, 23]);

    println!("input size: {}", input.len());

    for _ in 0..64 {
        for &input in input.iter() {
            ring.add_input(input);
        }
    }

    for element in ring.generate_dense_hash().iter().cloned() {
        print!("{:02x}", element);
    }

    println!("");
}
