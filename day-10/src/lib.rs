use std::ops::Range;
use std::ops::Index;

pub struct Ring {
    current_position: usize,
    skip_size: usize,
    elements: [u8; Ring::SIZE],
}

impl Default for Ring {
    fn default() -> Self {
        Ring {
            current_position: 0,
            skip_size: 0,
            elements: unsafe {
                let mut elements: [u8; Ring::SIZE] = std::mem::uninitialized();
                for (index, element) in elements.iter_mut().enumerate() {
                    *element = index as u8
                }
                elements
            },
        }
    }
}

impl Ring {
    const SIZE: usize = 256;
    const SPARSE_TO_DENSE_FACTOR: usize = 16;
    const DENSE_SIZE: usize = Ring::SIZE / Ring::SPARSE_TO_DENSE_FACTOR;

    pub fn add_input(&mut self, input: u8) {
        let to_reverse = self.current_position..self.current_position + input as usize;
        self.reverse_range(to_reverse);
        self.current_position =
            (self.current_position + self.skip_size + input as usize) % Ring::SIZE;
        self.skip_size = (self.skip_size + 1) % Ring::SIZE;
    }

    pub fn generate_dense_hash(&self) -> [u8; Ring::DENSE_SIZE] {
        unsafe {
            let mut dense_hash: [u8; Ring::DENSE_SIZE] = std::mem::uninitialized();
            let sparse_hash = &mut self.elements.iter().cloned();
            for element in dense_hash.iter_mut() {
                *element = sparse_hash
                    .take(Ring::SPARSE_TO_DENSE_FACTOR)
                    .fold(0, |fold, sparse| fold ^ sparse)
            }
            dense_hash
        }
    }

    fn reverse_range(&mut self, range: Range<usize>) {
        let reversed = range.clone().map(|index| index % Ring::SIZE).rev();
        let length = range.len();

        for (index1, index2) in range
            .map(|index| index % Ring::SIZE)
            .take(length / 2)
            .zip(reversed)
        {
            self.elements.swap(index1, index2);
        }
    }

    pub fn checksum(&self) -> usize {
        self.elements.iter().map(|&element| element as usize).sum()
    }
}

impl Index<usize> for Ring {
    type Output = u8;

    fn index(&self, index: usize) -> &Self::Output {
        &self.elements[index % Ring::SIZE]
    }
}
