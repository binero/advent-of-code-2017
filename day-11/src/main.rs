use std::str::{FromStr, from_utf8};
use std::cmp::{max, min};

fn main() {
    use std::io::{stdin, BufRead};

    let stdin = stdin();
    let input = stdin
        .lock()
        .split(b',')
        .map(|bytes| from_utf8(&bytes.unwrap()).unwrap().trim().parse().unwrap());

    let mut north: i32 = 0;
    let mut north_east: i32 = 0;
    let mut north_west: i32 = 0;

    let mut max_distance = 0;

    for direction in input {
        match direction {
            Direction::North => north += 1,
            Direction::NorthEast => north_east += 1,
            Direction::NorthWest => north_west += 1,
            Direction::South => north -= 1,
            Direction::SouthEast => north_west -= 1,
            Direction::SouthWest => north_east -= 1,
        }

        max_distance = max(
            max_distance,
            calculate_distance(north_west, north, north_east),
        )
    }

    println!("a: {}", calculate_distance(north_west, north, north_east));

    println!("b: {}", max_distance);
}

fn calculate_distance(north_west: i32, north: i32, north_east: i32) -> u32 {
    let mut distances = [
        max(0, north_west) as u32,
        max(0, north) as u32,
        max(0, north_east) as u32,
        max(0, -north_west) as u32,
        max(0, -north) as u32,
        max(0, -north_east) as u32,
    ];

    for index in 0..6 {
        let (left_index, middle_index, right_index) = (index, (index + 1) % 6, (index + 2) % 6);
        let delta = min(distances[left_index], distances[right_index]);
        distances[left_index] -= delta;
        distances[right_index] -= delta;
        distances[middle_index] += delta;
    }

    distances.iter().sum()
}

enum Direction {
    North,
    NorthEast,
    NorthWest,
    South,
    SouthEast,
    SouthWest,
}

impl FromStr for Direction {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "nw" => Ok(Direction::NorthWest),
            "ne" => Ok(Direction::NorthEast),
            "n" => Ok(Direction::North),
            "sw" => Ok(Direction::SouthWest),
            "se" => Ok(Direction::SouthEast),
            "s" => Ok(Direction::South),
            other => Err(format!("Unknown direction '{}'", other)),
        }
    }
}
