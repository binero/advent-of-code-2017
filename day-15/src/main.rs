fn main() {
    use std::env::args;
    let mut args = args().skip(1);
    let n1: u64 = args.next().unwrap().parse().unwrap();
    let n2: u64 = args.next().unwrap().parse().unwrap();

    println!("a: {}", a(n1, n2));
    println!("b: {}", b(n1, n2));
}

fn a(n1: u64, n2: u64) -> usize {
    let a = Generator {
        n: n1,
        factor: 16807,
    };
    let b = Generator {
        n: n2,
        factor: 48271,
    };

    let a = a.map(|n| (n & 0xFFFF) as u16);
    let b = b.map(|n| (n & 0xFFFF) as u16);

    a.zip(b).take(40000000).filter(|&(a, b)| a == b).count()
}

fn b(n1: u64, n2: u64) -> usize {
    let a = Generator {
        n: n1,
        factor: 16807,
    };
    let b = Generator {
        n: n2,
        factor: 48271,
    };

    let a = a.map(|n| (n & 0xFFFF) as u16).filter(|n| n % 4 == 0);
    let b = b.map(|n| (n & 0xFFFF) as u16).filter(|n| n % 8 == 0);

    a.zip(b).take(5000000).filter(|&(a, b)| a == b).count()
}

pub struct Generator {
    n: u64,
    factor: u64,
}

impl Iterator for Generator {
    type Item = u64;

    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        self.n = (self.n * self.factor) % 2147483647;
        Some(self.n)
    }
}
