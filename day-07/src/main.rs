use std::collections::HashMap;

struct Node {
    name: String,
    weight: u32,
    children_weight: u32,
    children: Vec<Node>,
}

impl Node {
    fn get_bad_weight(&self) -> Option<u32> {
        println!("- {}", self.name);
        for child in &self.children {
            println!("- - - {} - {}", child.name, child.get_total_weight());
        }
        let bad_child_and_good_wight = self.children
            .iter()
            .enumerate()
            .filter_map(|(index, child)| {
                self.children
                    .iter()
                    .enumerate()
                    .filter(|&(index2, child2)| {
                        index != index2 && child.get_total_weight() != child2.get_total_weight()
                    })
                    .skip(1)
                    .next()
                    .map(|(_, good_child)| (child, good_child.get_total_weight()))
            })
            .next();

        if let Some((bad_child, good_weight)) = bad_child_and_good_wight {
            if let Some(good_sub_weight) = bad_child.get_bad_weight() {
                Some(good_sub_weight)
            } else {
                let delta_weight = bad_child.get_total_weight() as i64 - good_weight as i64;

                Some(bad_child.weight - delta_weight as u32)
            }
        } else {
            None
        }
    }

    fn get_total_weight(&self) -> u32 {
        self.weight + self.children_weight
    }
}


fn main() {
    use std::io::{stdin, BufRead};

    let mut bases = HashMap::<String, String>::new();
    let mut nodes = HashMap::<String, (u32, Vec<String>)>::new();

    let stdin = stdin();
    let input = stdin.lock().lines().map(|line| {
        line.unwrap()
            .split_whitespace()
            .map(|word| {
                word.trim_matches(',')
                    .trim_matches('(')
                    .trim_matches(')')
                    .to_string()
            })
            .collect::<Vec<_>>()
    });

    for line in input {
        let mut line = line.into_iter();
        let base = (&mut line).next().unwrap();
        let weight = (&mut line).next().unwrap().parse().unwrap();
        let mut children = vec![];
        for top in line.skip(1) {
            bases.insert(top.clone(), base.clone());
            children.push(top);
        }
        nodes.insert(base, (weight, children));
    }

    let base = get_base(&bases).to_string();
    println!("a: {}", base);

    let tree = construct_tree(base, &mut nodes);
    println!("b: {}", tree.get_bad_weight().unwrap());
}

fn construct_tree(name: String, nodes: &mut HashMap<String, (u32, Vec<String>)>) -> Node {
    let (weight, names_of_children) = nodes.remove(&name).unwrap();
    let children = names_of_children
        .into_iter()
        .map(|child| construct_tree(child, nodes))
        .collect::<Vec<_>>();
    Node {
        name: name,
        weight: weight,
        children_weight: children
            .iter()
            .map(|node| node.get_total_weight())
            .sum::<u32>(),
        children: children,
    }
}

pub fn get_base(bases: &HashMap<String, String>) -> &str {
    let mut current_key = bases.keys().next().unwrap();

    while let Some(base) = bases.get(current_key) {
        current_key = base;
    }

    current_key
}
