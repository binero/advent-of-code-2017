#![feature(i128_type)]
use std::collections::VecDeque;

fn main() {
    use std::io::{stdin, BufRead};
    let stdin = stdin();
    let lines = stdin.lock().lines().map(Result::unwrap);
    let instructions: Vec<_> = lines.map(|line| Instruction::from_str(&line)).collect();

    {
        let mut cursor_position = 0;
        let mut computer = Computer::default();

        loop {
            if let Instruction::Receive(register) = instructions[cursor_position] {
                if computer.registers[register] != 0 {
                    println!("a: {}", computer.received_values.pop_back().unwrap());
                    break;
                }
                cursor_position += 1;
            } else {
                let (result, cursor_delta) =
                    computer.run_instruction(instructions[cursor_position]);
                if let Some(result) = result {
                    computer.received_values.push_back(result);
                }
                cursor_position = (cursor_position as isize + cursor_delta) as usize;
            }
        }
    }

    {
        let mut computer_a = Computer::default();
        let mut computer_b = Computer::default();
        computer_b.registers[parse_register("p").unwrap()] = 1;

        let mut cursor_a = 0;
        let mut cursor_b = 0;
        let mut sent_by_b = 0;

        loop {
            if !instructions[cursor_a].is_receive() || computer_a.received_values.len() > 0 {
                let (result, cursor_delta) = computer_a.run_instruction(instructions[cursor_a]);
                if let Some(result) = result {
                    computer_b.received_values.push_back(result);
                }
                assert!(cursor_a as isize + cursor_delta > 0);
                cursor_a = (cursor_a as isize + cursor_delta) as usize;
            } else {
                if !instructions[cursor_b].is_receive() || computer_b.received_values.len() > 0 {
                    if instructions[cursor_b].is_send() {
                        sent_by_b += 1;
                    }
                    let (result, cursor_delta) = computer_b.run_instruction(instructions[cursor_b]);
                    if let Some(result) = result {
                        computer_a.received_values.push_back(result);
                    }
                    assert!(cursor_b as isize + cursor_delta > 0);
                    cursor_b = (cursor_b as isize + cursor_delta) as usize;
                } else {
                    break;
                }
            }
        }

        println!("b: {}", sent_by_b);
    }
}

#[derive(Default)]
pub struct Computer {
    registers: [i128; 26],
    received_values: VecDeque<i128>,
}

impl Computer {
    pub fn run_instruction(&mut self, instruction: Instruction) -> (Option<i128>, isize) {
        match instruction {
            Instruction::Send(value) => return (Some(self.get_value(value)), 1),
            Instruction::Set(register, value) => self.registers[register] = self.get_value(value),
            Instruction::Add(register, value) => self.registers[register] += self.get_value(value),
            Instruction::Mul(register, value) => self.registers[register] *= self.get_value(value),
            Instruction::Mod(register, value) => self.registers[register] %= self.get_value(value),
            Instruction::Receive(register) => {
                let value = self.received_values.pop_front().unwrap();
                if value != 0 {
                    self.registers[register] = value;
                }
            }
            Instruction::Jump(value, delta) => if self.get_value(value) > 0 {
                return (None, self.get_value(delta) as isize);
            },
        }

        (None, 1)
    }

    fn get_value(&self, value: Value) -> i128 {
        match value {
            Value::Register(register) => self.registers[register],
            Value::Value(value) => value,
        }
    }
}

#[derive(Copy, Clone, Debug)]
pub enum Instruction {
    Send(Value),
    Set(usize, Value),
    Add(usize, Value),
    Mul(usize, Value),
    Mod(usize, Value),
    Receive(usize),
    Jump(Value, Value),
}

impl Instruction {
    pub fn from_str(s: &str) -> Instruction {
        let mut words = s.split_whitespace();

        match words.next() {
            Some("snd") => Instruction::Send(Value::from_str(words.next().unwrap())),
            Some("set") => Instruction::Set(
                parse_register(words.next().unwrap()).unwrap(),
                Value::from_str(words.next().unwrap()),
            ),
            Some("add") => Instruction::Add(
                parse_register(words.next().unwrap()).unwrap(),
                Value::from_str(words.next().unwrap()),
            ),
            Some("mul") => Instruction::Mul(
                parse_register(words.next().unwrap()).unwrap(),
                Value::from_str(words.next().unwrap()),
            ),
            Some("mod") => Instruction::Mod(
                parse_register(words.next().unwrap()).unwrap(),
                Value::from_str(words.next().unwrap()),
            ),
            Some("rcv") => Instruction::Receive(parse_register(words.next().unwrap()).unwrap()),
            Some("jgz") => Instruction::Jump(
                Value::from_str(words.next().unwrap()),
                Value::from_str(words.next().unwrap()),
            ),
            _ => panic!(),
        }
    }

    pub fn is_receive(&self) -> bool {
        if let &Instruction::Receive(_) = self {
            true
        } else {
            false
        }
    }

    pub fn is_send(&self) -> bool {
        if let &Instruction::Send(_) = self {
            true
        } else {
            false
        }
    }
}

#[derive(Copy, Clone, Debug)]
pub enum Value {
    Register(usize),
    Value(i128),
}

impl Value {
    fn from_str(s: &str) -> Value {
        parse_register(s)
            .map(|r| Value::Register(r))
            .unwrap_or_else(|| Value::Value(s.parse().unwrap()))
    }
}

pub fn parse_register(s: &str) -> Option<usize> {
    match s {
        "a" => Some(00),
        "b" => Some(01),
        "c" => Some(02),
        "d" => Some(03),
        "e" => Some(04),
        "f" => Some(05),
        "g" => Some(06),
        "h" => Some(07),
        "i" => Some(08),
        "j" => Some(09),
        "k" => Some(10),
        "l" => Some(11),
        "m" => Some(12),
        "n" => Some(13),
        "o" => Some(14),
        "p" => Some(15),
        "q" => Some(16),
        "r" => Some(17),
        "s" => Some(18),
        "t" => Some(19),
        "u" => Some(20),
        "v" => Some(21),
        "w" => Some(22),
        "x" => Some(23),
        "y" => Some(24),
        "z" => Some(25),
        _ => None,
    }
}
