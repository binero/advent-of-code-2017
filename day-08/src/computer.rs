use std::collections::HashMap;
use instruction::{Condition, Instruction};

pub struct Computer {
    registers: HashMap<String, i32>,
}

impl Default for Computer {
    fn default() -> Self {
        Computer {
            registers: HashMap::new(),
        }
    }
}

impl Computer {
    pub fn run_instruction(&mut self, instruction: Instruction) {
        if self.check_condition(&instruction.condition) {
            let register = self.registers.entry(instruction.register).or_insert(0);

            use instruction::Action;
            match instruction.action {
                Action::Increase(amount) => *register += amount,
                Action::Decrease(amount) => *register -= amount,
            }
        }
    }

    pub fn check_condition(&self, condition: &Condition) -> bool {
        let register_value = self.registers
            .get(&condition.register)
            .cloned()
            .unwrap_or(0);

        use instruction::Relation;
        match condition.relation {
            Relation::Equal => register_value == condition.constant,
            Relation::NotEqual => register_value != condition.constant,
            Relation::LessEqual => register_value <= condition.constant,
            Relation::GreaterEqual => register_value >= condition.constant,
            Relation::Less => register_value < condition.constant,
            Relation::Greater => register_value > condition.constant,
        }
    }

    pub fn greatest_register_value(&self) -> i32 {
        self.registers.values().cloned().max().unwrap_or(0)
    }
}
