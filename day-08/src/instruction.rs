use std::str::FromStr;
use regex::Regex;
use std::error::Error;
use std::fmt::{Display, Formatter, Result as FmtResult};

lazy_static! {
    static ref INSTRUCTION_REGEX: Regex = Regex::new(include_str!("regex.txt")).unwrap();
}

pub struct Instruction {
    pub register: String,
    pub action: Action,
    pub condition: Condition,
}

impl FromStr for Instruction {
    type Err = ParseError;

    fn from_str(instruction_str: &str) -> Result<Self, Self::Err> {
        let captures = INSTRUCTION_REGEX
            .captures(instruction_str)
            .ok_or_else(|| ParseErrorKind::Parse(instruction_str.to_string()))?;

        Ok(Instruction {
            register: captures.name("register").unwrap().as_str().to_string(),
            condition: Condition {
                register: captures.name("ifregister").unwrap().as_str().to_string(),
                relation: captures
                    .name("ifrelation")
                    .unwrap()
                    .as_str()
                    .parse()
                    .map_err(|error| {
                        ParseError::with_chain(
                            error,
                            ParseErrorKind::Parse(instruction_str.to_string()),
                        )
                    })?,
                constant: captures
                    .name("ifconstant")
                    .unwrap()
                    .as_str()
                    .parse()
                    .unwrap(),
            },
            action: Action::from_str_and_value(
                captures.name("action").unwrap().as_str(),
                captures.name("amount").unwrap().as_str().parse().unwrap(),
            ).map_err(|error| {
                ParseError::with_chain(error, ParseErrorKind::Parse(instruction_str.to_string()))
            })?,
        })
    }
}

impl Display for Instruction {
    fn fmt(&self, formatter: &mut Formatter) -> FmtResult {
        write!(
            formatter,
            "{} {} {}",
            self.register, self.action, self.condition
        )
    }
}

error_chain! {
    types {
        ParseError, ParseErrorKind, ResultExt;
    }

    errors {
        Parse(instruction_str: String) {
            description("invalid instruction string")
            display("`{}` is not a valid instruction.", instruction_str)
        }
    }
}

pub enum Action {
    Increase(i32),
    Decrease(i32),
}

pub struct Condition {
    pub register: String,
    pub relation: Relation,
    pub constant: i32,
}

impl Display for Condition {
    fn fmt(&self, formatter: &mut Formatter) -> FmtResult {
        write!(
            formatter,
            "{} {} {}",
            self.register, self.relation, self.constant
        )
    }
}

#[derive(Copy, Clone, Eq, PartialEq)]
pub enum Relation {
    Equal,
    LessEqual,
    GreaterEqual,
    Greater,
    Less,
    NotEqual,
}

impl FromStr for Relation {
    type Err = RelationParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "==" => Ok(Relation::Equal),
            "!=" => Ok(Relation::NotEqual),
            ">=" => Ok(Relation::GreaterEqual),
            "<=" => Ok(Relation::LessEqual),
            "<" => Ok(Relation::Less),
            ">" => Ok(Relation::Greater),
            unknown => Err(RelationParseError::new(unknown.to_string())),
        }
    }
}

impl Display for Relation {
    fn fmt(&self, formatter: &mut Formatter) -> FmtResult {
        match self {
            &Relation::Equal => write!(formatter, "=="),
            &Relation::NotEqual => write!(formatter, "!="),
            &Relation::GreaterEqual => write!(formatter, ">="),
            &Relation::LessEqual => write!(formatter, "<="),
            &Relation::Less => write!(formatter, "<"),
            &Relation::Greater => write!(formatter, ">"),
        }
    }
}

impl Action {
    pub fn from_str_and_value(string: &str, value: i32) -> Result<Action, ActionParseError> {
        match string {
            "dec" => Ok(Action::Decrease(value)),
            "inc" => Ok(Action::Increase(value)),
            other => Err(ActionParseError::new(other.to_string())),
        }
    }
}

impl Display for Action {
    fn fmt(&self, formatter: &mut Formatter) -> FmtResult {
        match self {
            &Action::Increase(amount) => write!(formatter, "inc {}", amount),
            &Action::Decrease(amount) => write!(formatter, "dec {}", amount),
        }
    }
}

#[derive(Debug)]
pub struct RelationParseError {
    pub string: String,
}

impl RelationParseError {
    fn new(string: String) -> Self {
        RelationParseError { string: string }
    }
}

impl Error for RelationParseError {
    fn description(&self) -> &str {
        "invalid relation string"
    }
}

impl Display for RelationParseError {
    fn fmt(&self, formatter: &mut Formatter) -> FmtResult {
        write!(formatter, "'{}' is not a valid relation.", self.string)
    }
}

#[derive(Debug)]
pub struct ActionParseError {
    pub string: String,
}

impl ActionParseError {
    fn new(string: String) -> Self {
        ActionParseError { string: string }
    }
}

impl Error for ActionParseError {
    fn description(&self) -> &str {
        "invalid action string"
    }
}

impl Display for ActionParseError {
    fn fmt(&self, formatter: &mut Formatter) -> FmtResult {
        write!(formatter, "'{}' is not a valid action.", self.string)
    }
}
