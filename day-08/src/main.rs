#[macro_use]
extern crate error_chain;
#[macro_use]
extern crate lazy_static;
extern crate regex;

mod computer;
mod instruction;

use computer::Computer;

fn main() {
    match run() {
        Ok((a, b)) => {
            println!("a: {}", a);
            println!("b: {}", b);
        }
        Err(error) => {
            use error_chain::ChainedError;
            eprintln!("{}", error.display_chain().to_string());
        }
    }
}

fn run() -> Result<(i32, i32)> {
    use std::io::{stdin, BufRead};

    let stdin = stdin();
    let lines = stdin.lock().lines();

    let mut computer = Computer::default();
    let mut greatest_value_ever = computer.greatest_register_value();

    for (index, line) in lines.enumerate() {
        let instruction = line?
            .parse()
            .map_err(|error| Error::with_chain(error, ErrorKind::Parse(index)))?;
        computer.run_instruction(instruction);

        use std::cmp::max;
        let greatest_value = computer.greatest_register_value();
        greatest_value_ever = max(greatest_value, greatest_value_ever);
    }

    Ok((computer.greatest_register_value(), greatest_value_ever))
}

error_chain! {
    foreign_links {
        Io(::std::io::Error);
    }

    errors {
        Parse(line: usize) {
            description("Failed to parse line")
            display("Failed to parse line {}.", line)
        }
    }
}
