extern crate day3;

use std::collections::HashMap;

const NEIGHBOURS: [(i64, i64); 8] = [
    (-1, -1),
    (-1, 0),
    (-1, 1),
    (0, -1),
    (0, 1),
    (1, -1),
    (1, 0),
    (1, 1),
];

fn main() {
    let input = day3::get_input();

    let mut grid: HashMap<(i64, i64), u32> = HashMap::new();
    grid.insert((0, 0), 1);

    let mut cx = 1;
    let mut cy = 0;

    let mut vx = 1;
    let mut vy = 0;

    loop {
        let (neighbour_count, sum) = NEIGHBOURS
            .iter()
            .cloned()
            .map(|(dx, dy)| (cx + dx, cy + dy))
            .filter_map(|neighbour| grid.get(&neighbour))
            .fold((0, 0), |(neighbour_count, sum), cell_value| {
                (neighbour_count + 1, sum + cell_value)
            });

        if sum > input {
            println!("{}", sum);
            break;
        } else {
            grid.insert((cx, cy), sum);

            {
                if (neighbour_count < 3) {
                    std::mem::swap(&mut vx, &mut vy);
                    vx = -vx;
                }
                cx += vx;
                cy += vy;
            }
        }

    }
}
