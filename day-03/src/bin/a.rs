extern crate day3;

pub fn main() {
    println!("{}", get_distance_to(day3::get_input()));

}

fn get_distance_to(location: u32) -> u32 {
    if (location == 1) {
        0
    } else {
        let mut current_positions = [1, 1, 1, 1];
        let mut walking_distances = [0, 0, 0, 0];
        let mut minimum_distance = location;

        let (number_distance, walking_distance) = (1..)
            .map(|distance| distance * 2 - 1)
            .enumerate()
            .map(|(index, dist)| (index % 4, dist))
            .map(move |(index, number_distance)| {
                current_positions[index] += number_distance;
                walking_distances[index] += 1;

                (current_positions[index], walking_distances[index])
            })
            .map(|(current_position, walking_distance)| {
                (
                    (current_position - location as i64).abs() as u32,
                    walking_distance,
                )
            })
            .take_while(|&(number_distance, _)| if number_distance <
                minimum_distance
            {
                minimum_distance = number_distance;
                true
            } else {
                false
            })
            .last()
            .unwrap();

        walking_distance + number_distance
    }
}

#[test]
fn test() {
    assert_eq!(get_distance_to(1), 0);
    assert_eq!(get_distance_to(12), 3);
    assert_eq!(get_distance_to(23), 2);
    assert_eq!(get_distance_to(1024), 31);
}
