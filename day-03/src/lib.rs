pub fn get_input() -> u32 {
    use std::env::args;

    args()
        .skip(1)
        .next()
        .expect("Did not get any input.")
        .parse()
        .expect("The input was not a number")
}
