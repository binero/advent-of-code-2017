#![feature(test)]

extern crate day4;
extern crate smallvec;
extern crate test;

use test::{black_box, Bencher};
use smallvec::SmallVec;

const INPUTS: [(&'static str, usize, usize); 2] = [
    (include_str!("../inputs/1.txt"), 325, 119),
    (include_str!("../inputs/2.txt"), 383, 265),
];

#[bench]
fn bench_a_real(bencher: &mut Bencher) {
    bencher.iter(|| {
        for &(input, result, _) in black_box(&INPUTS) {
            assert_eq!(result, day4::a(input.lines()));
        }
    });
}

#[bench]
fn bench_b_real(bencher: &mut Bencher) {
    bencher.iter(|| {
        for &(input, _, result) in black_box(&INPUTS) {
            assert_eq!(result, day4::b(input.lines()));
        }
    });
}


#[bench]
fn bench_a(bencher: &mut Bencher) {
    bencher.iter(|| {
        for &(input, result, _) in black_box(&INPUTS) {
            assert_eq!(result, a(input.lines()));
        }
    });
}

#[bench]
fn bench_b(bencher: &mut Bencher) {
    bencher.iter(|| {
        for &(input, _, result) in black_box(&INPUTS) {
            assert_eq!(result, b(input.lines()));
        }
    });
}

pub fn a<'a, I>(lines: I) -> usize
where
    I: Iterator<Item = &'a str>,
{
    let lines = lines.map(|line| {
        line.split_whitespace().collect::<SmallVec<[_; 16]>>()
    });

    lines
        .filter(|line| {
            line.iter()
                .enumerate()
                .filter(|&(index, ref word1)| {
                    line.iter()
                        .skip(index + 1)
                        .filter(|word2| word1 == word2)
                        .next()
                        .is_some()
                })
                .next()
                .is_none()
        })
        .count()
}


pub fn b<'a, I>(lines: I) -> usize
where
    I: Iterator<Item = &'a str>,
{
    let lines = lines.map(|line| {
        line.split_whitespace()
            .map(|word| word.chars().collect::<SmallVec<[_; 8]>>())
            .map(|mut word| {
                word.sort();
                word
            })
            .collect::<SmallVec<[_; 16]>>()
    });

    let lines = lines.filter(|sorted_words| {
        sorted_words
            .iter()
            .enumerate()
            .flat_map(|(index, word)| {
                sorted_words
                    .iter()
                    .skip(index + 1)
                    .map(move |word2| (word, word2))
            })
            .filter(|&(ref word, ref word2)| word == word2)
            .next()
            .is_none()
    });

    lines.count()
}
