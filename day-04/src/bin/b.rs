extern crate day4;

fn main() {
    use std::io::{stdin, BufRead};

    let stdin = stdin();
    let lines = stdin.lock().lines().map(|line_res| line_res.unwrap());

    println!("{}", day4::b(lines));

}
