extern crate smallvec;

use smallvec::SmallVec;
use std::borrow::Borrow;

pub fn a<B, I>(lines: I) -> usize
where
    I: Iterator<Item = B>,
    B: Borrow<str>,
{
    let lines = lines.map(|line| {
        line.borrow()
            .split_whitespace()
            .map(String::from)
            .collect::<SmallVec<[_; 16]>>()
    });

    lines
        .filter(|line| {
            line.iter()
                .enumerate()
                .filter(|&(index, ref word1)| {
                    line.iter()
                        .skip(index + 1)
                        .filter(|word2| word1 == word2)
                        .next()
                        .is_some()
                })
                .next()
                .is_none()
        })
        .count()
}

pub fn b<B, I>(lines: I) -> usize
where
    I: Iterator<Item = B>,
    B: Borrow<str>,
{
    let lines = lines.map(|line| {
        line.borrow()
            .split_whitespace()
            .map(|word| word.chars().collect::<SmallVec<[_; 8]>>())
            .map(|mut word| {
                word.sort();
                word
            })
            .collect::<SmallVec<[_; 16]>>()
    });

    let lines = lines.filter(|sorted_words| {
        sorted_words
            .iter()
            .enumerate()
            .flat_map(|(index, word)| {
                sorted_words
                    .iter()
                    .skip(index + 1)
                    .map(move |word2| (word, word2))
            })
            .filter(|&(ref word, ref word2)| word == word2)
            .next()
            .is_none()
    });

    lines.count()
}
